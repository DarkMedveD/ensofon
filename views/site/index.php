<?php
/* @var $this app\components\View */

$this->title = 'Главная страница';

$this->registerScssFile("/css/site/index.scss");

$this->registerJsFile("/js/site/index.js", [
    'depends' => \yii\web\JqueryAsset::class
])

?>

<div id="index-page">

</div>